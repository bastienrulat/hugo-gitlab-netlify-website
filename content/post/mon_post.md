---

---
# Un post

Le contenu de ce premier post.

Cet ensemble de test vise à voir les possibilités induites par la combinaison d'Hugo en tant que SSG, forestry.io en tant que CMS, Netlify en tant que CDN.

Pour en savoir plus sur ces différents outils, commencez par parcourir leurs pages officielles respectives

* \[Hugo\]([https://gohugo.io](https://gohugo.io "https://gohugo.io"))
* \[Gitlab\](htttps://gitlab.com)
* \[Netlify\]([https://www.netlify.com](https://www.netlify.com "https://www.netlify.com"))